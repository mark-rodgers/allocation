$( document ).ready(function() {
	init();
	$( ".slider" ).slider({
		min: 0,
		max: 100,
		value: 50,
		slide: function( event, ui ) {
				var current = this.parentNode;
				var total = $(this.parentNode).find(".total").val();
				var p1 = 100 - ui.value;
				var p2 = ui.value;
				var a1 = total * ( p1 / 100 );
				var a2 = total * ( p2 / 100 );

				$(this).parent().find(".percentage1").attr("value", p1 + "%" );
				$(this).parent().find( ".percentage1" ).attr("value", p1 + "%" );
				$(this).parent().find( ".percentage2" ).attr("value", p2 + "%" );
				$(this).parent().find( ".amount1" ).attr("value", "$" + a1.toFixed(2) );
				$(this).parent().find( ".amount2" ).attr("value", "$" + a2.toFixed(2) );
				calculateTotals();
		},
		create: function( event, ui ) {
			var value = $(this).parent().find(".total").val();
			$(this).parent().find(".percentage1").attr("value", "50%");
			$(this).parent().find(".percentage2").attr("value", "50%");
			$(this).parent().find(".amount1").attr("value", "$"+ value/2);
			$(this).parent().find(".amount2").attr("value", "$"+ value/2);
		}
	});
});

function init() {
	var sum = 0;
	$('.total').each(function(){
		var val = parseFloat(this.value.replace("$", ""));
		sum += val;
	});

	sum = sum/2;

	$('#person1').val(sum.toFixed(2));
	$('#person2').val(sum.toFixed(2));
}

function calculateTotals() {
	var sum1 = 0;
	var sum2 = 0;
	$('.amount1').each(function(){
		var val = parseFloat(this.value.replace("$", ""));
		sum1 += val;
	});
	$('.amount2').each(function(){
		var val = parseFloat(this.value.replace("$", ""));
		sum2 += val;
	});

	$('#person1').val(sum1.toFixed(2));
	$('#person2').val(sum2.toFixed(2));
}